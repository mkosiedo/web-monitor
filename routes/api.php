<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['as' => 'api.', 'namespace' => "Api"], function () {
    Route::group(['as' => 'monitor.', 'namespace' => "Monitor"], function () {
        Route::post("/monitors", "MonitorApiController@add_website")->name("add_url");
        Route::get("/monitors/{url}", "MonitorApiController@get_stats")
            ->where('url', '(.*)')
            ->name("get_stats");
    });
});
