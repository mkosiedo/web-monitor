<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class MonitorAddTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testConvertStrToBool()
    {
        $tested_values = [
            [ 'true'  => true],
            [ 'True'  => true],
            [ '1'     => true],
            [ 'false' => false],
            [ 'False' => false],
            [ '0'     => false],
            [ 'foo'   => false],
            [ ''      => false],
        ];
        foreach ($tested_values as $value){

            $this->assertTrue(convert_str_to_bool(array_keys($value)[0]) === array_values($value)[0]);
        }

    }
}
