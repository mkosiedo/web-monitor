<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MonitorAddFeatureTest extends TestCase
{

    protected $test_data =  ['urls' =>
        [
        'https://onet.pl/',
        "https://www.google.pl"
        ]
    ];


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {

        $response = $this->postJson('/monitors', $this->test_data);

        $response
            ->assertStatus(200)
            ->assertJson([
                'code' => 200,
                'data' => [],
            ]);

    }

    public function testNegativeInputWithoutHttp()
    {
        $status_code = 422;
        $response = $this->postJson('/monitors', ['urls' =>
            [
                'onet.pl',
                "www.google.pl"
            ]
        ]);

        $response
            ->assertStatus($status_code)
            ->assertJson([
                'code' => $status_code,
                'data' => [],
            ]);

    }

    public function testNegativeMissingInput()
    {
        $status_code = 422;
        $response = $this->postJson('/monitors');

        $response
            ->assertStatus($status_code)
            ->assertJson([
                'code' => $status_code,
                'data' => [],
            ]);
    }

    public function testNegativeInvalidInput()
    {
        $status_code = 422;
        $response = $this->postJson('/monitors', ['urls' =>
            [
                "aa"
            ]
        ]);

        $response
            ->assertStatus($status_code)
            ->assertJson([
                'code' => $status_code,
                'data' => [],
            ]);
    }


    public function testHomepage()
    {
        $response = $this->get("/");
        $response->assertStatus(200);

    }
}
