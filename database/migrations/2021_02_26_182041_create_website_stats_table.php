<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsiteStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_stats', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("website_id");
            $table->boolean("success");
            $table->double("total_time"); # s
            $table->double("redirect_time"); # s
            $table->unsignedInteger("redirect_count");

            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->softDeletes();

            # INDEX
            $table->foreign('website_id')->references('website_id')->on('websites');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_stats');
    }
}
