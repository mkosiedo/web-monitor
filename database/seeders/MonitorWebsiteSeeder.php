<?php

namespace Database\Seeders;

use App\Interfaces\WebsiteInterface;
use Illuminate\Database\Seeder;

class MonitorWebsiteSeeder extends Seeder
{
    protected $website_interface;

    public function __construct(WebsiteInterface $website_interface)
    {
        $this->website_interface = $website_interface;

    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $websites = [
            ["url" => "https://onet.pl/"],
            ["url" => "http://socialmention.com/"],
            ["url" => "http://test-redirects.137.software"],
            ["url" => "https://google.com"]
        ];

        foreach ($websites as $website)
        {
            $this->website_interface->create($website);
        }

    }
}
