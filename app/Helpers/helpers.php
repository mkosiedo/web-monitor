<?php
if (! function_exists('convert_str_to_bool')) {
    function convert_str_to_bool($value): bool
    {
       return (boolean)json_decode(strtolower($value)); # to convert "Yes" etc. to bool
    }
}
