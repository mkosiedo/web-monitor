<?php


namespace App\Transformers;


trait CustomArrayTransformers
{
    public function transformArrayKeyAsField(array $data, string $key): array
    {
        for ($i=0; $i<count($data); $i++)
        {
            $data[array_keys($data)[$i]][$key] = array_keys($data)[$i];
        }
        return array_values($data);
    }

    public function reshapeArray($data, string $column): array
    {
        return $data->pluck($column)->toArray();
    }
}
