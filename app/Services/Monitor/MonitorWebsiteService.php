<?php


namespace App\Services\Monitor;


use App\Interfaces\WebsiteInterface;
use App\Interfaces\WebsiteStatInterface;
use App\Jobs\ExecuteMeasureJob;


class MonitorWebsiteService
{

    protected $website_interface;
    protected $website_stats_interface;

    public function __construct(WebsiteInterface $website_interface,
                                WebsiteStatInterface $website_stats_interface)
    {
        $this->website_interface = $website_interface;
        $this->website_stats_interface = $website_stats_interface;
    }

    protected function getWorkersCount(): int
    {
        return env("MONITOR_WORKERS", 4);
    }



    public function executeMeasure(float $timeout=null)
    {
        # get urls for n runners
        # make measure
        # save results to DB

        $url_for_execute = $this->website_interface->getUrlsForMeasure($this->getWorkersCount());
        foreach ($url_for_execute as $urls)
        {
            if(!count($urls)) continue;

            dispatch(new ExecuteMeasureJob($urls, $timeout))
                ->onQueue("monitor");
        }

    }

    public function getXStats(): array
    {
        $urls = array_values($this->website_interface->all()->pluck("url")->toArray());
        $response = array_combine($urls, array_fill(0, count($urls), null));
        $this->executeMeasure(2);

        $stats = $this->website_stats_interface->getAllStats(2);

        return array_merge($response, $stats);
    }


}
