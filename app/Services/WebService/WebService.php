<?php


namespace App\Services\WebService;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\TransferStats;


class WebService
{
    const CONCURRENCY = 100;
    protected $client;
    protected $measure;
    protected $measured_times =[];
    protected $timeout=30;

    public function __construct()
    {

        $this->client = new Client($this->getOptions(null));
    }

    protected function getOptions($url_id): array
    {
        $options =  [
            'http_errors'     => false,
            'timeout'         => $this->timeout,
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0'
            ],
            'allow_redirects' => [
                'track_redirects' => true,
                'max'             => 100,
            ]
        ];
        if($url_id != null){
            $options['on_stats'] = function (TransferStats $stats) use ($url_id) {
                if($stats->hasResponse())
                {
                    $this->measured_times[$url_id][] = $stats->getTransferTime();
                }
            };
        }
        return $options;
    }

    protected function getRedirectCount(Response $response): int
    {
        return count($response->getHeader(\GuzzleHttp\RedirectMiddleware::HISTORY_HEADER));
    }

    protected function getTotalRequestTime(int $id): float
    {
        return array_sum($this->measured_times[$id]);
    }

    protected function getRedirectRequestTime(int $id, int $redirect_cnt): float
    {
        return array_sum(
            array_slice($this->measured_times[$id], 0, $redirect_cnt)
        );
    }

    protected function getMeasureForResponse(bool $success, Response $response=null): array
    {
        $measured_data = [
            "total_time" => 0,
            "redirect_time" => 0,
            "redirect_count" => 0,
            "success" => $success,
        ];
        if($success){
            $measured_data["redirect_count"] = $this->getRedirectCount($response);
        }
        return $measured_data;

    }

    public function setTimeout(float $value)
    {
        $this->timeout = $value;
    }

    public function measureRedirects(array $urls_with_keys, float $timeout=null): array
    {
        if($timeout != null)
        {
            $this->setTimeout($timeout);
        }

        $this->measure = [];
        $requests = function () use ($urls_with_keys){
            foreach ($urls_with_keys as $id => $url) {
                yield function() use ($id, $url) {
                    return $this->client->getAsync($url, $this->getOptions($id));
                };
            }
        };

        $pool = new Pool($this->client, $requests(), [
            'concurrency' => self::CONCURRENCY,
            'fulfilled' => function (Response $response, $index)  {

                $this->measure[$index] = $this->getMeasureForResponse(true, $response);
            },
            'rejected' => function (ConnectException $reason, $index) {
                $this->measure[$index] = $this->getMeasureForResponse(false);
            },
        ]);
        $promise = $pool->promise();
        $promise->wait();
        ksort($this->measure);
        assert(count($this->measure) == $urls_with_keys );

        # map to requested urls keys
        $measured_data= [];
        for ($i=0; $i< count($urls_with_keys); $i++)
        {
            $measured_data[array_keys($urls_with_keys)[$i]] = $this->measure[$i];
        }

        for ($i=0; $i<count($measured_data); $i++)
        {
            $key = array_keys($measured_data)[$i];
            $measured_data[$key]["total_time"] = $this->getTotalRequestTime($key);
            $measured_data[$key]["redirect_time"] = $this->getRedirectRequestTime($key, $measured_data[$key]["redirect_count"]);
        }

        return $measured_data;

    }
}
