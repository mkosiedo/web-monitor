<?php

namespace App\Jobs;

use App\Services\Monitor\MonitorWebsiteService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ScheduleMeasureJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param MonitorWebsiteService $monitor_website_service
     * @return void
     */
    public function handle(MonitorWebsiteService $monitor_website_service)
    {
        $monitor_website_service->executeMeasure();
    }
}
