<?php

namespace App\Jobs;

use App\Interfaces\WebsiteStatInterface;
use App\Services\WebService\WebService;
use App\Transformers\CustomArrayTransformers;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ExecuteMeasureJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, CustomArrayTransformers;

    protected $urls;
    protected $timeout;

    /**
     * Create a new job instance.
     *
     * @param array $urls
     * @param float $timeout
     */
    public function __construct(array $urls, float $timeout=null)
    {
        $this->urls = $urls;
        $this->timeout = $timeout;
    }

    /**
     * Execute the job.
     *
     * @param WebService $web_service
     * @param WebsiteStatInterface $website_stat_interface
     * @return void
     */
    public function handle(WebService $web_service,  WebsiteStatInterface $website_stat_interface)
    {
        $collectd_data= $web_service->measureRedirects($this->urls, $this->timeout);

        $status = $website_stat_interface
            ->batchCreate(
                $this->transformArrayKeyAsField($collectd_data, "website_id"),
                ["total_time", "redirect_time", "redirect_count", "success", "website_id"]
            );
        #TODO validate status
    }
}
