<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebsiteStat extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $primaryKey= "id";
    protected $fillable = [
        "website_id",
        "success",
        "total_time",
        "redirect_time",
        "redirect_count"
    ];

    public $response_columns = ["success", "total_time", "redirect_time", "redirect_count", "created_at"];


    public function website(): BelongsTo
    {
        return $this->belongsTo(Website::class, "website_id", "website_id");
    }
}
