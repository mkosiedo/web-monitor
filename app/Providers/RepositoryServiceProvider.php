<?php

namespace App\Providers;

use App\Interfaces\BaseInterface;
use App\Interfaces\WebsiteInterface;
use App\Interfaces\WebsiteStatInterface;
use App\Repository\Eloquent\BaseEloquentRepository;
use App\Repository\Eloquent\WebsiteStatRepository;
use App\Repository\Eloquent\WebsiteRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(BaseInterface::class, BaseEloquentRepository::class);
        $this->app->bind(WebsiteInterface::class, WebsiteRepository::class);
        $this->app->bind(WebsiteStatInterface::class, WebsiteStatRepository::class);
    }
}
