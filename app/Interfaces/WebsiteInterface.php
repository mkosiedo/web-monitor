<?php


namespace App\Interfaces;


interface WebsiteInterface extends BaseEloquentInterface
{

    public function getUrlsForMeasure(int $chunks): array;

}
