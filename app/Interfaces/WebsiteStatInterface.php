<?php


namespace App\Interfaces;


interface WebsiteStatInterface extends BaseInterface
{
    public function getStatsForUrl(string $url, int $seconds): array;

    public function getAllStats(int $seconds): array;
}
