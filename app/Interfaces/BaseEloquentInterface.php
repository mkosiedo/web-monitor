<?php


namespace App\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface BaseEloquentInterface extends BaseInterface
{

    public function create(array $attributes): Model;
    public function batchCreate(array $values, array $columns= null, int $batchSize = 500): bool;
    public function find($id): ?Model;
    public function all($columns=null): Collection;
}
