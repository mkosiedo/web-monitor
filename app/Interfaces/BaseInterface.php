<?php


namespace App\Interfaces;


interface BaseInterface
{

    public function create(array $attributes);
    public function batchCreate(array $values, array $columns=null, int $batchSize = 500): bool;
    public function find($id);
}
