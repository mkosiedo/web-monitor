<?php

namespace App\Http\Controllers\Api\Monitor;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\Monitor\AddWebsiteToMonitorRequest;
use App\Http\Resources\Api\Monitor\WebsiteResource;
use App\Http\Resources\Api\Monitor\WebsiteStatResource;
use App\Interfaces\WebsiteInterface;
use App\Interfaces\WebsiteStatInterface;


use App\Services\Monitor\MonitorWebsiteService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;


class MonitorApiController extends ApiController
{

    protected $website_interface;
    protected $website_stat_interface;
    protected $monitor_website_service;

    public function __construct(WebsiteInterface $website_interface,
                                WebsiteStatInterface $website_stat_interface,
                                MonitorWebsiteService $monitor_website_service)
    {
        $this->website_interface = $website_interface;
        $this->website_stat_interface = $website_stat_interface;
        $this->monitor_website_service = $monitor_website_service;

    }

    /**
     * @param AddWebsiteToMonitorRequest $request
     * @return JsonResponse
     */

    public function add_website(AddWebsiteToMonitorRequest $request): JsonResponse

    {

        $valid_data = $request->validated();
        $stats = false;
        if($request->query->has("stats") && isset($valid_data["stats"]))
        {
            $stats = convert_str_to_bool($valid_data["stats"]);
        }

        if($stats)
        {
            $headers = [
                "X-Stats" => json_encode($this->monitor_website_service->getXStats())
            ];
        }
        else
        {
            $headers = [];
        }

        $urls= [];
        foreach ($valid_data["urls"] as $url)
        {
            $urls[] = [$url];
        }

        $status = $this->website_interface->batchCreate($urls);

        return (new WebsiteResource())->responseFromBoolStatus($status, [], $headers);

    }


    public function get_stats($website_url): JsonResponse
    {
        $validator = Validator::make(["url" => $website_url], [
            'url' => 'required|url|string|max:65535',

        ]);
        if ($validator->fails()) {
            return (new WebsiteStatResource())->errorResponse(Response::HTTP_UNPROCESSABLE_ENTITY);
        }



        $data = $this->website_stat_interface->getStatsForUrl($website_url, 600*60);

        return (new WebsiteStatResource())->successResponse($data, []);
    }


}
