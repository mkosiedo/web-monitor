<?php

namespace App\Http\Requests\Api\Monitor;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Support\Facades\Auth;


class AddWebsiteToMonitorRequest extends BaseApiRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {


        return [
            "urls" => "required|array",
            "urls.*" => "required|url|string|max:65535", # protocol is required
            "stats" => "nullable|boolean_with_str"
        ];
    }
}
