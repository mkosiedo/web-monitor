<?php

namespace App\Http\Requests\Api;

use App\Http\Resources\Api\ApiResource;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;


class BaseApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *te
     * @return bool
     */
    public function authorize()
    {
        return $this->isJson() && $this->expectsJson(); # Auth::user()->can..  # I don`t implement authorization in demo app
    }



    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        throw new HttpResponseException(
            (new ApiResource())->rawErrorResponse($errors, Response::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

}
