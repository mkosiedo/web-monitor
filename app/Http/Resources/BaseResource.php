<?php


namespace App\Http\Resources;

use Illuminate\Http\JsonResponse;

class BaseResource
{
    /**
     * Core of response
     *
     * @param string $message
     * @param array|object $data
     * @param ?array $headers
     * @param integer $statusCode
     * @param boolean $isSuccess
     * @return JsonResponse
     */
    protected function coreResponse(string $message,
                                 $data = [],
                                 array $headers = [],
                                 int $statusCode= 200,
                                 $isSuccess = true): JsonResponse
    {
        // Check the params
        if (!$message) return response()->json(['message' => ['Message is required']], 500);

        $request_data["code"] = $statusCode;
        $request_data["data"] = (object)$data;
        $request_data[ $isSuccess ? 'messages' : 'errors'] = [$message];

        return response()->json($request_data, $statusCode, $headers);
    }

    /**
     * Send any success response
     *
     * @param string $message
     * @param array|object $data
     * @param ?array $headers
     * @param integer $statusCode
     * @return JsonResponse
     */
    public function rawSuccessResponse(string $message,
                            $data,
                            array $headers = [],
                            int $statusCode = 200): JsonResponse
    {
        return $this->coreResponse($message, $data, $headers, $statusCode);
    }

    /**
     * Send any error response
     *
     * @param string $message
     * @param integer $statusCode
     * @return JsonResponse
     */
    public function rawErrorResponse(string $message,
                          int $statusCode = 500): JsonResponse
    {
        return $this->coreResponse($message, [], [], $statusCode, false);
    }

}
