<?php

namespace App\Http\Resources\Api;


use App\Http\Resources\BaseResource;
use Illuminate\Http\JsonResponse;

class ApiResource extends BaseResource
{

    protected $success_message = null;
    protected $error_message = null;

    public function successResponse($data, array $headers, int $statusCode=200): JsonResponse
    {
        if($this->success_message === null) return $this->rawErrorResponse("Success message is not set");

        return $this->rawSuccessResponse($this->success_message, $data, $headers, $statusCode);
    }


    public function errorResponse($statusCode=500):JsonResponse
    {
        if($this->success_message === null) return $this->rawErrorResponse("Error message is not set");

        return $this->rawErrorResponse($this->error_message, $statusCode);
    }


    public function responseFromBoolStatus(bool $status,
                                           $data=[],
                                           array $headers=[],
                                           int $statusCode=200): JsonResponse
    {
        if($status)
        {
            return $this->successResponse($data, $headers, $statusCode);
        }
        else
        {
            return $this->errorResponse();
        }
    }
}
