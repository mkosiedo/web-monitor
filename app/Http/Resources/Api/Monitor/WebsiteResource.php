<?php

namespace App\Http\Resources\Api\Monitor;

use App\Http\Resources\Api\ApiResource;


class WebsiteResource extends ApiResource
{
    protected $success_message = "Urls added with success";
    protected $error_message = "Unable to add new urls to monitor";
}
