<?php

namespace App\Http\Resources\Api\Monitor;

use App\Http\Resources\Api\ApiResource;


class WebsiteStatResource extends ApiResource
{
    protected $success_message = "List of monitored website with stats for last 10 minutes";
    protected $error_message = "Invalid url param, you should use url with http:// or https://";
}
