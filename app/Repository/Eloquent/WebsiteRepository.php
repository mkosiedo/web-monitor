<?php


namespace App\Repository\Eloquent;


use App\Interfaces\WebsiteInterface;
use App\Models\Website;


class WebsiteRepository extends BaseEloquentRepository implements WebsiteInterface
{

    public function __construct(Website $model)
    {
        parent::__construct($model);
    }

    public function getUrlsForMeasure(int $chunks): array
    {
        $total = $this
            ->model
            ->select($this->model->primaryKey)
            ->count();
        $number_of_elements = (int)ceil($total / $chunks);

        $chunked_data = [];
        for ($i=0; $i<$chunks; $i++)
        {
            $urls= $this
                ->model
                ->skip($number_of_elements*$i)
                ->take($number_of_elements)
                ->get(["website_id","url"]);

            $data = [];
            foreach ($urls as $url)
            {
                $data[$url["website_id"]] = $url["url"];
            }
            $chunked_data[] = $data;
        }

        return $chunked_data;
    }
}
