<?php


namespace App\Repository\Eloquent;


use App\Interfaces\BaseEloquentInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class BaseEloquentRepository implements BaseEloquentInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    public function batchCreate(array $values, array $columns= null, int $batchSize = 500): bool
    {

        $instance = new $this->model();
        if(!$columns)
        {
            $columns = $this->model->getFillable();
        }

        $status =  batch()->insert($instance, $columns, $values, $batchSize);

        return is_array($status);
    }

    public function find($id): ?Model
    {
        return $this->model->find($id);
    }

    public function all($columns=null): Collection
    {
        if ($columns)
        {
            return $this->model->all($columns);
        }
        return $this->model->all();
    }

}
