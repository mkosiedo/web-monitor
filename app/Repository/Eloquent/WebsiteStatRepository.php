<?php


namespace App\Repository\Eloquent;


use App\Interfaces\WebsiteStatInterface;
use App\Models\WebsiteStat;
use App\Transformers\CustomArrayTransformers;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;


class WebsiteStatRepository extends BaseEloquentRepository implements WebsiteStatInterface
{
    use CustomArrayTransformers;

    public function __construct(WebsiteStat $model)
    {
        parent::__construct($model);
    }

    protected function transformForChart($columns, $data)
    {
        $response_data = [];
        foreach ($columns as $column){
            $response_data[$column] = $this->reshapeArray($data, $column);
        }
        return $response_data;
    }

    public function getStatsForUrl(string $url, int $seconds): array
    {

        $stats = $this->model->whereHas("website",  function (Builder $query) use ($url) {
            $query->where('url', $url);
        })
            ->where("created_at", ">=", Carbon::now()->subSeconds($seconds))
            ->get($this->model->response_columns);


        # reshape data for better use with charts etc

        return $this->transformForChart($this->model->response_columns, $stats);
    }

    public function getAllStats(int $seconds): array
    {
        $stats = $this->model
            ->where("created_at", ">=", Carbon::now()->subSeconds($seconds))
            ->with("website")
            ->get()
            ->groupBy("website.url");

        $data = [];
        foreach ($stats as $key => $value)
        {
            $data[$key] = $this->transformForChart($this->model->response_columns, $value);
        }
        return $data;
    }

}
